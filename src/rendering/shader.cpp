#include <GL/glew.h>
#include <iostream>

#include "shader.h"



void hll::shader::use(unsigned int shader)
{
    glUseProgram(shader);
}

unsigned int hll::shader::compile(const char *vert_src, const char *frag_src, const char *geo_src)
{
    unsigned int vert_shader, frag_shader, geo_shader;

    vert_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vert_shader, 1, &vert_src, nullptr);
    glCompileShader(vert_shader);
    hll::shader::check_compile_errors(vert_shader, "VERTEX");

    frag_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(frag_shader, 1, &frag_src, nullptr);
    glCompileShader(frag_shader);
    hll::shader::check_compile_errors(frag_shader, "FRAGMENT");

    if (geo_src != nullptr)
    {
        geo_shader = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(geo_shader, 1, &geo_src, nullptr);
        glCompileShader(geo_shader);
        hll::shader::check_compile_errors(geo_shader, "GEOMETRY");
    }

    unsigned int program = glCreateProgram();
    glAttachShader(program, vert_shader);
    glAttachShader(program, frag_shader);

    if (geo_src != nullptr) {
        glAttachShader(program, geo_shader);
    }

    glLinkProgram(program);
    hll::shader::check_compile_errors(program, "PROGRAM");

    glDeleteShader(vert_shader);
    glDeleteShader(frag_shader);

    if (geo_src != nullptr) {
        glDeleteShader(geo_shader);
    }

    return program;
}

void hll::shader::check_compile_errors(unsigned int obj, const std::string &type)
{
    int success;
    char info_log[1024];

    if (type != "PROGRAM")
    {
        glGetShaderiv(obj, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(obj, 1024, nullptr, info_log);
            std::cout << "| ERROR::SHADER: Compile-time error: Type: " << type << "\n"
                      << info_log << "\n -- --------------------------------------------------- -- "
                      << std::endl;
        }
    }
    else
    {
        glGetProgramiv(obj, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(obj, 1024, nullptr, info_log);
            std::cout << "| ERROR::Shader: Link-time error: Type: " << type << "\n"
                      << info_log << "\n -- --------------------------------------------------- -- "
                      << std::endl;
        }
    }
}

void hll::shader::set_int(unsigned int shader, const char *name, int val)
{
    glUniform1i(glGetUniformLocation(shader, name), val);
}

void hll::shader::set_float(unsigned int shader, const char *name, float val)
{
    glUniform1f(glGetUniformLocation(shader, name), val);
}

void hll::shader::set_vec_2f(unsigned int shader, const char *name, float x, float y)
{
    glUniform2f(glGetUniformLocation(shader, name), x, y);
}

void hll::shader::set_vec_2f(unsigned int shader, const char *name, const glm::vec2 &val)
{
    glUniform2f(glGetUniformLocation(shader, name), val.x, val.y);
}

void hll::shader::set_vec_3f(unsigned int shader, const char *name, float x, float y, float z)
{
    glUniform3f(glGetUniformLocation(shader, name), x, y, z);
}

void hll::shader::set_vec_3f(unsigned int shader, const char *name, const glm::vec3 &val)
{
    glUniform3f(glGetUniformLocation(shader, name), val.x, val.y, val.z);
}

void hll::shader::set_vec_4f(unsigned int shader, const char *name, float x, float y, float z, float w)
{
    glUniform4f(glGetUniformLocation(shader, name), x, y, z, w);
}

void hll::shader::set_vec_4f(unsigned int shader, const char *name, const glm::vec4 &val)
{
    glUniform4f(glGetUniformLocation(shader, name), val.x, val.y, val.z, val.w);
}

void hll::shader::set_mat_4f(unsigned int shader, const char *name, glm::mat4 &val)
{
    glUniformMatrix4fv(glGetUniformLocation(shader, name), 1, false, glm::value_ptr(val));
}


