#ifndef HELLENGINE_TEXTURE_2_D_H
#define HELLENGINE_TEXTURE_2_D_H


namespace hll::tex_2d {
    struct TexInfo
    {
        unsigned int width, height;
        unsigned int format_internal;
        unsigned int format_img;
        unsigned int wrap_s;
        unsigned int wrap_t;
        unsigned int filter_min;
        unsigned int filter_mag;
    };

    unsigned int
    generate(const TexInfo &info, unsigned int width, unsigned int height, unsigned char *data);
    void bind(unsigned int tex);
}



#endif //HELLENGINE_TEXTURE_2_D_H