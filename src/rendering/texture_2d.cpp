#include "texture_2d.h"

#include <GL/glew.h>



unsigned int hll::tex_2d::generate(const TexInfo &info, unsigned int width, unsigned int height, unsigned char *data)
{
    unsigned int tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D, 0, info.format_internal, width, height, 0, info.format_img, GL_UNSIGNED_BYTE, data);

    glTexParameteri(GL_TEXTURE, GL_TEXTURE_WRAP_S, info.wrap_s);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_WRAP_T, info.wrap_t);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, info.filter_min);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, info.filter_mag);

    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    return tex;
}

void hll::tex_2d::bind(unsigned int tex)
{
    glBindTexture(GL_TEXTURE_2D, tex);
}

