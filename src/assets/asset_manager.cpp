#include <string>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <utility>
#include <filesystem>

#include <GL/glew.h>
#include <stb_image/stb_image.cpp>

#include "asset_manager.h"
#include "../rendering/shader.h"


std::map<std::string, unsigned int> hll::AssetManager::shaders;
std::map<std::string, unsigned int> hll::AssetManager::textures;
std::map<std::string, hll::tex_2d::TexInfo> hll::AssetManager::texture_infos;


unsigned int hll::AssetManager::load_shader(const char *vert_file_path, const char *frag_file_path, const char *geo_file_path, std::string name) {
    const auto shader = load_shader_from_file(vert_file_path, frag_file_path, geo_file_path);
    shaders[name] = shader;
    return shader;
}

unsigned int hll::AssetManager::get_shader(std::string name) {
    return shaders[name];
}

hll::tex_2d::TexInfo hll::AssetManager::load_tex_2d(const char *file_path, bool alpha, std::string name) {
    auto res = load_tex_2d_from_file(file_path, alpha);
    textures[name] = res.first;
    texture_infos[name] = res.second;

    return texture_infos[name];
}

unsigned int hll::AssetManager::get_tex_2d(std::string name) {
    return textures[name];
}

hll::tex_2d::TexInfo hll::AssetManager::get_tex_2d_info(std::string name) {
    return texture_infos[name];
}

bool hll::AssetManager::tex_2d_is_loaded(std::string name) {
    return hll::AssetManager::textures.contains(name);
}

void hll::AssetManager::clear() {
    for (auto iter: shaders) {
        glDeleteProgram(iter.second);
    }

    for (auto iter: textures) {
        glDeleteTextures(1, &iter.second);
    }
}

bool hll::AssetManager::is_valid_path(const char *file_path) {
    return std::filesystem::exists(file_path);
}



hll::AssetManager::AssetManager() {}

unsigned int hll::AssetManager::load_shader_from_file(const char *vert_file_path, const char *frag_file_path, const char *geo_file_path) {
    std::string vert_code;
    std::string frag_code;
    std::string geo_code;

    try {
        // open files
        std::ifstream vert_file_stream(vert_file_path);
        std::ifstream frag_file_stream(frag_file_path);

        std::stringstream vert_str_stream, frag_str_stream;
        vert_str_stream << vert_file_stream.rdbuf();
        frag_str_stream << frag_file_stream.rdbuf();

        vert_file_stream.close();
        frag_file_stream.close();

        vert_code = vert_str_stream.str();
        frag_code = frag_str_stream.str();

        if (geo_file_path != nullptr) {
            std::ifstream geo_file_stream(geo_file_path);
            std::stringstream geo_str_stream;
            geo_str_stream << geo_file_stream.rdbuf();
            geo_file_stream.close();
            geo_code = geo_str_stream.str();
        }
    }
    catch (std::exception e) {
        std::cout << "ERROR::SHADER: Failed to read shader files" << std::endl;
    }

    return hll::shader::compile(
            vert_code.c_str(),
            frag_code.c_str(),
            (geo_file_path != nullptr ? geo_code.c_str() : nullptr));
}

std::pair<unsigned int, hll::tex_2d::TexInfo> hll::AssetManager::load_tex_2d_from_file(const char *file, bool alpha) {
    const unsigned int format = alpha ? GL_RGBA : GL_RGB;

    tex_2d::TexInfo info{
            .format_internal = format,
            .format_img = format,
            .wrap_s = GL_REPEAT,
            .wrap_t = GL_REPEAT,
            .filter_min = GL_LINEAR,
            .filter_mag = GL_LINEAR,
    };

    if (alpha) {
        info.format_internal = GL_RGBA;
        info.format_img = GL_RGBA;
    }

    int width, height, nr_channels;
    unsigned char *data = stbi_load(file, &width, &height, &nr_channels, 0);
    auto tex = hll::tex_2d::generate(info, width, height, data);
    stbi_image_free(data);

    return std::make_pair(tex, info);
}
