
#ifndef HELLENGINE_BALL_OBJECT_H
#define HELLENGINE_BALL_OBJECT_H

#include "game_object.h"

namespace hll {

class BallObject : public hll::GameObject {
public:
    float radius;
    bool is_stuck;

public:
    BallObject();
    BallObject(glm::vec2 pos, float rad, glm::vec2 vel, unsigned int tex);

    glm::vec2 move(float dt, unsigned int window_width);
    void reset(glm::vec2 pos, glm::vec2 vel);
};

}


#endif //HELLENGINE_BALL_OBJECT_H
