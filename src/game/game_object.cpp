#include "game_object.h"



hll::GameObject::GameObject()
    : pos(0.0f, 0.0f), size(1.0f, 1.0f), vel(0.0f), color(1.0f), rot(0.0f), tex(), is_solid(false), is_destroyed(false) {}

hll::GameObject::GameObject(glm::vec2 pos, glm::vec2 size, unsigned int tex, glm::vec3 color, glm::vec2 vel)
    : pos(pos), size(size), vel(vel), color(color), rot(0.0f), tex(tex), is_solid(false), is_destroyed(false) {}

void hll::GameObject::draw(const sprite_renderer::RenderInfo &render_info) const {
    hll::sprite_renderer::draw_sprite(render_info, this->tex, this->pos, this->size, this->rot, this->color);
}