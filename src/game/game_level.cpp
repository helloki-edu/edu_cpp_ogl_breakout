#include <fstream>
#include <sstream>
#include <utility>

#include "../assets/asset_manager.h"
#include "game.h"
#include "game_level.h"

hll::GameLevel::GameLevel() {}

void hll::GameLevel::load(const char *file_path, unsigned int level_width, unsigned int level_height) {
    this->bricks.clear();
    this->file_path = file_path;
    this->width = level_width;
    this->height = level_height;

    this->reload();
}

void hll::GameLevel::reload() {
    unsigned int tile_code;
    std::string line;
    std::ifstream fstream(file_path);
    std::vector<std::vector<unsigned int>> tile_data;

    if (fstream) {
        while (std::getline(fstream, line)) {
            std::istringstream sstream(line);
            std::vector<unsigned int> row;

            while (sstream >> tile_code) {
                row.push_back(tile_code);
            }
            tile_data.push_back(row);
        }

        if (tile_data.size() > 0) {
            this->init(tile_data);
        }
    }
}

void hll::GameLevel::draw(const hll::sprite_renderer::RenderInfo &render_info) const {
    for (auto &tile: this->bricks) {
        if (!tile.is_destroyed) {
            tile.draw(render_info);
        }
    }
}

bool hll::GameLevel::is_completed() {
    for (auto &tile: this->bricks) {
        if (!tile.is_solid && !tile.is_destroyed) {
            return false;
        }
    }

    return true;
}

void hll::GameLevel::init(std::vector<std::vector<unsigned int>> tile_data) {
    unsigned int height = tile_data.size();
    unsigned int width = tile_data[0].size();

    float unit_width = this->width / static_cast<float>(width);
    float unit_height = this->height / height;

    for (unsigned int y = 0; y < height; y++) {
        for (unsigned int x = 0; x < width; x++) {
            auto curr_data = tile_data[y][x];

            if (curr_data == 1) {
                glm::vec2 pos((unit_width * x), (unit_height * y));
                glm::vec2 size(unit_width, unit_height);
                GameObject obj(
                        pos, size,
                        hll::AssetManager::get_tex_2d(hll::game::BLOCK_SOLID_NAME),
                        glm::vec3(0.8f, 0.8f, 0.7f), glm::vec3(1.0f));
                obj.is_solid = true;

                this->bricks.push_back(obj);

            } else if (curr_data > 1) {
                glm::vec3 color = glm::vec3(1.0f);
                if (curr_data == 2) {
                    color = glm::vec3(0.2f, 0.6f, 1.0f);
                } else if (curr_data == 3) {
                    color = glm::vec3(0.0f, 0.7f, 0.0f);
                } else if (curr_data == 4) {
                    color = glm::vec3(0.8f, 0.8f, 0.4f);
                } else if (curr_data == 5) {
                    color = glm::vec3(1.0f, 0.5f, 0.0f);
                }

                glm::vec2 pos((unit_width * x), (unit_height * y));
                glm::vec2 size(unit_width, unit_height);

                this->bricks.emplace_back(
                        pos, size, hll::AssetManager::get_tex_2d(hll::game::BLOCK_NAME),
                        color, glm::vec2(0.0));
            }
        }
    }
}